def margined(fill: str, margin: int, background: str = " ") -> str:
    return f"{background * margin}{fill}{background * margin}"


def padded(border: str, padding: int, background: str = " ") -> str:
    if padding >= 0:
        return f"{border}{background * padding}{border}"
    else:
        return f"{border}{border[-padding::]}"


def wrapped(fill: str, width: int, background: str = " ") -> str:
    return padded(fill, width - 2 * len(fill), background)


def triangle_width(triangle_height):
    return 2 * triangle_height - 1


def upper_triangles(
        base_triangle_height: int,
        fill: str = "*",
        background: str = " "
) -> str:
    base_triangle_width = triangle_width(base_triangle_height)
    upper_triangle_height = (base_triangle_height + 1) // 2
    upper_triangle_width = triangle_width(upper_triangle_height)

    lines: list[str] = []

    for line in range(upper_triangle_height):
        triangle_layer = (fill * (2 * line + 1)).center(upper_triangle_width)
        curr_line = wrapped(triangle_layer, base_triangle_width, background)

        lines.append(curr_line)

    return "\n".join(lines)


def base_triangle(
        base_triangle_height: int,
        name: str,
        fill: str = "*",
        background: str = " "
) -> str:
    base_triangle_width = triangle_width(base_triangle_height)
    lines: list[str] = []

    for line in range(base_triangle_height):
        curr_line: str = ""

        if line == 0:
            curr_line = margined(
                name.center(base_triangle_width - 2),
                1,
                fill
            )
        else:
            curr_line = margined(
                wrapped(fill, base_triangle_width - 2 * line, background),
                line,
                background
            )

        lines.append(curr_line)

    return "\n".join(lines)


def heart(
        base_triangle_height: int,
        name: str,
        fill: str = "*",
        background: str = " "
) -> str:
    return upper_triangles(
        base_triangle_height,
        fill,
        background
    ) + "\n" + base_triangle(
        base_triangle_height,
        name,
        fill,
        background
    )


if __name__ == "__main__":
    HEART_NAME = "matmon"
    base_triangle_height = int(input("Enter a number: "))

    print(heart(base_triangle_height, HEART_NAME))
